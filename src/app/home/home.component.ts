import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }

navigateToAddAssetType(){

this.router.navigate(['addassettype']);

}

navigateToAddAssetDetail(){
  this.router.navigate(['addassetdetail']);
}

navigateToRoleManagement(){

this.router.navigate(['rolemanagement']);

}
navigateToAllocDealloc(){

this.router.navigate(['allocdealloc']);

}

navigateToAddEmployee(){
  this.router.navigate(['addemployee']);
}

navigateToEditEmployee(){
  this.router.navigate(['editemployee']);
}

navigateToAddAssetModel() {
  this.router.navigate(['addassetmodel'])
}
navigateToHome() {
  this.router.navigate([''])
}

}
