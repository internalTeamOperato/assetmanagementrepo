import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../services/employee.service';

@Component({
  selector: 'app-addemployee',
  templateUrl: './addemployee.component.html',
  styleUrls: ['./addemployee.component.css'],
  providers: [EmployeeService]
})
export class AddemployeeComponent implements OnInit {

employee:employee = {};
employmentType:string;
employmentStatus:string;
employmentTypeMap : Map<number, string> = new Map<number, string>();
employmentStatusMap : Map<number, string> = new Map<number, string>();

  constructor(public router: Router,public employeeService: EmployeeService) { }

  ngOnInit() {
    this.employmentTypeMap.set(1,'Permanent');
    this.employmentTypeMap.set(2,'Temporary');
    this.employmentStatusMap.set(1,'Active');
    this.employmentStatusMap.set(2,'Inactive');
    
    
  }

  backtohome(){

    this.router.navigate(['']);
  }

  addemployee():void{
    
  this.employee.employmentType = this.employmentTypeMap.get(parseInt(this.employmentType));
  this.employee.employmentStatus = this.employmentStatusMap.get(parseInt(this.employmentStatus));
console.log(this.employee);
  this.employeeService.addEmployee(this.employee).subscribe(response=>{
console.log('The response after adding' + response);
 });
  this.employee = {};
  }

}

export interface employee{

	employeeId?:string;
	ntId?:string;
  uId?:number;
  employeeName?:string;
  departmentName?:string;
  departmentNumber?:string;
  managerName?:string;
  emailId?:string;
  employmentType?:string;
  dateOfJoining?:Date;
	managerEmployeeId?:string;
	employmentStatus?:string;
	designation?:string;
	idCreationDate?:Date;
  extensionNumber?:string;
	cubicleLocation?:string;
}