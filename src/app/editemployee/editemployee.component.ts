import {OnInit,Component,Output,EventEmitter} from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import {employee} from "../addemployee/addemployee.component";
import { trigger, style, transition, animate, group,state }  from '@angular/animations';

@Component({
  selector: 'app-editemployee',
  templateUrl: './editemployee.component.html',
  styleUrls: ['./editemployee.component.css'],
  providers: [EmployeeService],
 animations: [
    trigger('myAwesomeAnimation', [
        
        transition('small <=> large', animate('400ms ease', style({
          transform: 'rotateX(180deg)'
        }))),
        
    ]),
     trigger('showAnimation', [
      state('show', style({ opacity: 1 })),
      state('hidden', style({ opacity: 0 })),
      transition('show => hidden', animate('300ms')),
      transition('hidden => show', animate('300ms')),
    ]) 
  ]
})
export class EditemployeeComponent implements OnInit {
 //@Output() onLoadEvent: EventEmitter<string> = new EventEmitter();
employeeList:employee[]=[];
employeeObj:employee={};
basicPaneleditable:boolean;
otherDetailsPaneleditable:boolean;
state: string = 'small';
showState:string = 'hidden';
showEmployeePanel:boolean;
showMultiSearch:boolean=false;
employeeNameSrchStr:string;

employeeObjPanel:employee={};

  constructor(public employeeService: EmployeeService) { }

  ngOnInit() {
   // this.onLoadEvent.emit("Load event started..");
  }

  

  searchEmployee(){
   
 let responseEmployeeList:employee[]=[];
 responseEmployeeList = this.employeeList;
  this.employeeService.searchEmployee(this.employeeObj)
  .subscribe((responseEmployeeList:employee[])=>{
   
     this.employeeList=responseEmployeeList;
      console.log(this.employeeList);
});


  }


    searchEmployeeOnName(){
   
 let responseEmployeeList:employee[]=[];
 //responseEmployeeList = this.employeeList;
 this.employeeList=[];
 if(''!=this.employeeNameSrchStr){
  this.employeeService.searchEmployeeOnName(this.employeeNameSrchStr)
  .subscribe((responseEmployeeList:employee[])=>{
   this.employeeList=responseEmployeeList;
      console.log(this.employeeList);
});
 }
  }

editBasicPanel(){

this.state = (this.state === 'small' ? 'large' : 'small');
this.basicPaneleditable = true;
  }
saveBasicPanelDetails(){
 this.state = (this.state === 'small' ? 'large' : 'small');
setTimeout(()=>{this.basicPaneleditable = false; }, 400);

}

canceleditBasicPanel(){
 this.state = (this.state === 'small' ? 'large' : 'small');
setTimeout(()=>{this.basicPaneleditable = false; }, 400);
}

viewEmployee(index){
  this.employeeObjPanel = this.employeeList[index];
  this.showState = (this.showState === 'hidden' ? 'show' : 'hidden');
this.showEmployeePanel=true;
}

editOtherDetailsPanel(){
this.state = (this.state === 'small' ? 'large' : 'small');
this.otherDetailsPaneleditable = true;
}
saveOtherDetailsPanel(){
   this.state = (this.state === 'small' ? 'large' : 'small');
setTimeout(()=>{this.otherDetailsPaneleditable = false; }, 400);
}
cancelOtherDetailsPanel(){
   this.state = (this.state === 'small' ? 'large' : 'small');
setTimeout(()=>{this.otherDetailsPaneleditable = false; }, 400);
}

displayMultiSearch(){
 
  this.showMultiSearch = true ;
}

displayNameSearch(){
  this.showMultiSearch = false ;
 
}

disableInput(event:any){
event.target.readOnly = true;
event.target.value = "";
}
enableInput(event:any){
event.target.readOnly = false;
event.target.disabled = false;

}



}

