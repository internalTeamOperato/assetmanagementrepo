import { LoginService } from './services/login.service';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { error } from 'util';

@Injectable()
export class UserService {


  private isUserLoggedIn;
  public username;

  constructor(public ls: LoginService) { 
    this.isUserLoggedIn = false;
  }

  validateUser(username: string, password: string){
    if(this.ls.loginCheck(username,password)) {
      this.setUserLoggedIn(username);
      return true;
    }
    return false;
  }

  private setUserLoggedIn(username) {
    this.isUserLoggedIn = true;
    this.username = username;
  }

  getUserLoggedIn() {
    return this.isUserLoggedIn;
  }

}
