import { Component, OnInit,ViewChild,ElementRef,AfterViewInit  } from '@angular/core';
import {FormControl} from '@angular/forms';
import {employee} from "../addemployee/addemployee.component";
import { assetMasterData } from "../addassettype/addassettype.component";
import { AssetModel } from "../addassetmodel/addassetmodel.component";
import { EmployeeService } from '../services/employee.service';

import { AddAssetTypeService } from '../services/addassettype.service';
import { AddAssetModelService } from "../services/addassetmodel.service";
import { AllocateService } from "../services/allocate.service";

@Component({
  selector: 'app-allocdealloc',
  templateUrl: './allocdealloc.component.html',
  styleUrls: ['./allocdealloc.component.css'],
  providers: [EmployeeService,AddAssetTypeService,AddAssetModelService,AllocateService],
})
export class AllocdeallocComponent implements OnInit,AfterViewInit  {
  assetTypeOvrLay:string;
  assetModelOvrLay:string;
  assetSerialNumber:string;
  assetTypeList:assetMasterData[]=[];
  assetModelList:AssetModel[]=[];
 
  employeeList:employee[]=[];
  employeeObj:employee={};
  allocatedAssetsList:allocatedAsset[]=[];

  
  

  // TODO: @ViewChild('overlayDivId') 
  //overlayDiv: ElementRef;
  allocDivClass:any;

  constructor(public employeeService: EmployeeService,private addassettypeservice:AddAssetTypeService,
    private addAssetModelService:AddAssetModelService,private allocateService:AllocateService) {   }
 
  ngOnInit() {
  }

  ngAfterViewInit(){
    this.assetTypeOvrLay = "Choose Asset Type"

    this.assetModelOvrLay = "Choose Asset Model"
    let responseAssetList:assetMasterData[]=[];
   this.addassettypeservice.getAssets().subscribe(responseAssetList=>{
     console.log(responseAssetList);
     this.assetTypeList = responseAssetList;
 });  
 
 let responseAssetModelList:AssetModel[]=[];
 this.addAssetModelService.getAssetModels().subscribe(responseAssetList=>{
   console.log(responseAssetList);
   this.assetModelList = responseAssetList;
});     
   }

 searchEmployee(){
   
 let responseEmployeeList:employee[]=[];
 responseEmployeeList =  this.employeeList;
  this.employeeService.searchEmployee(this.employeeObj)
  .subscribe((responseEmployeeList:employee[])=>{
   
     this.employeeList=responseEmployeeList;
      console.log(this.employeeList);
      let uid = this.employeeList[0].uId;
this.allocateService.getAllocations(uid).subscribe(responseAssetList=>{
  console.log(responseAssetList);
  this.allocatedAssetsList = responseAssetList;
});
});



  }


  /**To allocate assets to employee   */
  onclickAllocate(){
 
 //TODO: Later this.allocDivClass = "allocateDivExpanded";
  document.getElementById("allocateDiv").style.width = "94.5%";

this.assetTypeOvrLay = "Choose Asset Type"
this.assetSerialNumber = "";
  }

  allocateAsset(){
    let allocTemp:allocatedAsset={};
   allocTemp.uid = this.employeeList[0].uId;
   allocTemp.assetId = -1;
    allocTemp.slNo = this.assetSerialNumber;

    this.allocateService.allocateAsset(allocTemp).subscribe(response=>{
      console.log('The response after adding' + response);
       });


    this.allocatedAssetsList.push(allocTemp);
    document.getElementById("allocateDiv").style.width = "0";
    document.body.style.backgroundColor = "white";
  }

  closePanel(){
    document.getElementById("allocateDiv").style.width = "0";
    document.body.style.backgroundColor = "white";
  }
}

export interface allocatedAsset{

	assetId?:number;
  slNo?:string;
  uid?:number;
}