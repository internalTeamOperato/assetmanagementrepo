import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllocdeallocComponent } from './allocdealloc.component';

describe('AllocdeallocComponent', () => {
  let component: AllocdeallocComponent;
  let fixture: ComponentFixture<AllocdeallocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllocdeallocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllocdeallocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
