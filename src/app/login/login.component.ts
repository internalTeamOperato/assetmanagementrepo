
import { UserService } from './../user.service';
import { Component, OnInit } from '@angular/core';
import { Routes, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 
  username:string;
  password: string;
  constructor(public router:Router, public userService: UserService) {

   }

  ngOnInit() {

    
  }
  authenticateUser():(void){

    //his.username = 
    let auth = this.userService.validateUser(this.username, this.password);
    if(auth) {
      this.router.navigate(['home']);
    } else {
      alert("Invalid user");
    }
  }
 
}
