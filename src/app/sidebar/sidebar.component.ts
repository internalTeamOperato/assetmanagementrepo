import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }
navigateToAddAssetType(){

this.router.navigate(['addassettype']);

}

navigateToRoleManagement(){

this.router.navigate(['rolemanagement']);

}
navigateToAllocDealloc(){

this.router.navigate(['allocdealloc']);

}

navigateToAddEmployee(){
  this.router.navigate(['addemployee']);
}

navigateToEditEmployee(){
  this.router.navigate(['editemployee']);
}

navigateToAddAssetModel() {
  this.router.navigate(['addassetmodel'])
}

navigateToHome() {
  this.router.navigate([''])
}
}
