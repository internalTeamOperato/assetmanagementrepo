import { Injectable } from '@angular/core';
 import { Http, Response } from "@angular/http";
 import { Headers, RequestOptions } from '@angular/http';
 import "rxjs/add/operator/map";
 import { Observable } from "rxjs";
 import { allocatedAsset } from "../allocdealloc/allocdealloc.component";


 @Injectable()
 export class AllocateService{
  
     constructor(private http: Http){
         console.log('Allocate Service Initialized...');
    }


    allocateAsset(allocated:allocatedAsset): Observable<allocatedAsset> {
       
        let headers = new Headers({ 'Content-Type': 'application/json' });
        headers.append('Access-Control-Allow-Headers', 'Content-Type');
       headers.append('Access-Control-Allow-Methods', 'POST');
       headers.append('Access-Control-Allow-Origin', '*');
           let options = new RequestOptions( {method: "post", headers: headers
            , url : "http://localhost:8080/AssetManagement/AssetAllocation/addAllocation"});
    
           var jsonAssetData = JSON.stringify(allocated);
   
       return this.http.post("http://localhost:8080/AssetManagement/AssetAllocation/addAllocation",
        jsonAssetData, options).map(res => res.json());
                  
   } 
   
   getAllocations(uid:number){
       return this.http.get('http://localhost:8080/AssetManagement/AssetAllocation/listAllocation/'+uid)
       .map(res => res.json());
   }


}