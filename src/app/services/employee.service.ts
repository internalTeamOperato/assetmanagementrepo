import { Injectable } from '@angular/core';
import { Http, Response } from "@angular/http";
import { Headers, RequestOptions } from '@angular/http';
import "rxjs/add/operator/map";
import { Observable } from "rxjs";
import {employee} from "../addemployee/addemployee.component";

@Injectable()
export class EmployeeService{
  
    constructor(private http: Http){
        console.log('Employee Service Initialized...');
    }


     addEmployee(employee:employee): Observable<employee> {
       
     let headers = new Headers({ 'Content-Type': 'application/json' });
     headers.append('Access-Control-Allow-Headers', 'Content-Type');    
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
        let options = new RequestOptions( {method: "post", headers: headers
         , url : "http://localhost:8080/AssetManagement/Employee/addEmployee"});
 
        var jsonAssetData = JSON.stringify(employee);

    return this.http.post("http://localhost:8080/AssetManagement/Employee/addEmployee",
     jsonAssetData, options).map(res => res.json());
               
} 

 searchEmployee(employee:employee){
       
     let headers = new Headers({ 'Content-Type': 'application/json' });
     headers.append('Access-Control-Allow-Headers', 'Content-Type');    
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
        let options = new RequestOptions( {method: "post", headers: headers
         , url : "http://localhost:8080/AssetManagement/Employee/searchEmployee"});
 
        var jsonData = JSON.stringify(employee);

    return this.http.post("http://localhost:8080/AssetManagement/Employee/searchEmployee",
     jsonData, options).map(res => res.json());
               
} 

searchEmployeeOnName(name:string){
       
     let headers = new Headers({ 'Content-Type': 'application/json' });
     headers.append('Access-Control-Allow-Headers', 'Content-Type');    
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
        let options = new RequestOptions( {method: "post", headers: headers
         , url : "http://localhost:8080/AssetManagement/Employee/searchEmployeeOnName"});
 
       // var jsonData = JSON.stringify(name);

    return this.http.post("http://localhost:8080/AssetManagement/Employee/searchEmployeeOnName",
     name+'%', options).map(res => res.json());
               
} 
}