import { Injectable } from '@angular/core';
import { Http, Response } from "@angular/http";
import { Headers, RequestOptions } from '@angular/http';
import { AddassettypeComponent } from "../addassettype/addassettype.component";
import "rxjs/add/operator/map";
import { Observable } from "rxjs";
import {assetMasterData} from "../addassettype/addassettype.component";

@Injectable()
export class AddAssetTypeService{
  
    constructor(private http: Http){
        console.log('AddAssetType Service Initialized...');
    }

   addAssetType(assetMasterData:assetMasterData): Observable<assetMasterData> {
       
     let headers = new Headers({ 'Content-Type': 'application/json' });
     headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
        let options = new RequestOptions( {method: "post", headers: headers
         , url : "http://localhost:8081/assetType"});
 
        var jsonAssetData = JSON.stringify(assetMasterData);
console.info(jsonAssetData);
    return this.http.post("http://localhost:8081/assetType",
     jsonAssetData, options).map(res => res.json());
               
} 

getAssets(){
    return this.http.get('http://localhost:8081/assetType/complete')
    .map(res => res.json());
}

  private extractData(res: Response) {
	let body = res.json();
        return body.data || {};
    }
    private handleErrorObservable (error: Response | any) {
	console.error(error.message || error);
	return Observable.throw(error.message || error);
    }
}