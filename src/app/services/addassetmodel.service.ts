 import { Injectable } from '@angular/core';
 import { Http, Response } from "@angular/http";
 import { Headers, RequestOptions } from '@angular/http';
 import "rxjs/add/operator/map";
 import { Observable } from "rxjs";
 import {AssetModel} from "../addassetmodel/addassetmodel.component";

 @Injectable()
 export class AddAssetModelService{
  
     constructor(private http: Http){
         console.log('Asset Model Service Initialized...');
    }

   addAssetModel(assetModel:AssetModel): Observable<AssetModel> {
       
     let headers = new Headers({ 'Content-Type': 'application/json' });
      headers.append('Access-Control-Allow-Headers', 'Content-Type');     headers.append('Access-Control-Allow-Methods', 'POST');
     headers.append('Access-Control-Allow-Origin', '*');
        let options = new RequestOptions( {method: "post", headers: headers
          , url : "http://localhost:8080/AssetManagement/AssetModel/addAssetModel"});
 
        var jsonAssetModelData = JSON.stringify(assetModel);

    return this.http.post("http://localhost:8080/AssetManagement/AssetModel/addAssetModel",
      jsonAssetModelData, options).map(res => res.json());
               
 } 
 getAssetModels(){
    return this.http.get('http://localhost:8080/AssetManagement/AssetModel/listAssetModel')
     .map(res => res.json());
 }

   private extractData(res: Response) {
 	let body = res.json();
        return body.data || {};
    }
    private handleErrorObservable (error: Response | any) {
 	console.error(error.message || error);
	return Observable.throw(error.message || error);
    }
 }