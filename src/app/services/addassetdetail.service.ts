import { Injectable } from '@angular/core';
import { Http, Response } from "@angular/http";
import { Headers, RequestOptions } from '@angular/http';
import "rxjs/add/operator/map";
import { Observable } from "rxjs";
import {assetdetail} from "../addassetdetail/addassetdetail.component";

@Injectable()
export class AssetDetailService{
  
    constructor(private http: Http){
        console.log('Asset Detail Service Initialized...');
    }


     addAssetDetail(assetdetail:assetdetail): Observable<assetdetail> {
       
     let headers = new Headers({ 'Content-Type': 'application/json' });
     headers.append('Access-Control-Allow-Headers', 'Content-Type');    
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
        let options = new RequestOptions( {method: "post", headers: headers
         , url : "http://localhost:8081/addAssetDetail"});
 
        var jsonAssetData = JSON.stringify(assetdetail);

    return this.http.post("http://localhost:8081/addAssetDetail",
     jsonAssetData, options).map(res => res.json());
               
} 
getAsset(){
    return this.http.get('http://localhost:8081/assetDetail/complete')
     .map(res => res.json());
 }

  
}