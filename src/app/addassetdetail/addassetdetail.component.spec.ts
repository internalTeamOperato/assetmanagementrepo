import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddassetdetailComponent } from './addassetdetail.component';

describe('AddassetdetailComponent', () => {
  let component: AddassetdetailComponent;
  let fixture: ComponentFixture<AddassetdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddassetdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddassetdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
