import { Component, OnInit, AfterViewInit , TemplateRef} from '@angular/core';

import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AssetDetailService } from '../services/addassetdetail.service';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

@Component({
  selector: 'app-addassetdetail',
  templateUrl: './addassetdetail.component.html',
  styleUrls: ['./addassetdetail.component.css'],
  providers: [AssetDetailService]
})
export class AddassetdetailComponent implements OnInit, AfterViewInit {

  assetdetail:assetdetail = {};
  osType:string;
  modelType:string;
  assetSta:String;
  osTypeMap : Map<number, string> = new Map<number, string>();
  modelTypeMap : Map<number, string> = new Map<number, string>();
  assetStaMap : Map<number, string> = new Map<number, string>();

  assetList = [];
  assetDetailList:assetdetail[]=[];
  assetModal: assetdetail ={};
  public modalRef: BsModalRef;

   constructor(public router: Router,private modelService: BsModalService,public assetDetailService: AssetDetailService) { }

  ngOnInit() {

    this.osTypeMap.set(0,'32 bit');
    this.osTypeMap.set(1,'64 bit');
    this.modelTypeMap.set(1,'1');
    this.assetStaMap.set(0, 'Active');
    this.assetStaMap.set(1, 'Inactive');
    
  }

  backtohome(){

    this.router.navigate(['']);
  }

  addAssetDetail():void{
    
    console.log(this.assetdetail.assetId);
    console.log(this.assetdetail.modelType);
    console.log(this.assetdetail.serialNumber);
    console.log(this.assetdetail.hostName);
    console.log(this.assetdetail.osType);
    console.log(this.assetdetail.assetModelId);
    console.log(this.assetdetail.dualBand);
    console.log(this.assetdetail.imeiNo);
    console.log(this.assetdetail.ramUpgrade);
    console.log(this.assetdetail.ramSerialNo);
    console.log(this.assetdetail.ramPartNo);
    console.log(this.assetdetail.assetStatus);
    console.log(this.assetdetail.vendor);
    console.log(this.assetdetail.fiscalYear);
    console.log(this.assetdetail.complaint)
    console.log(this.assetdetail.comments);

    var newAsset:assetdetail = {};

    newAsset.assetId=this.assetdetail.assetId;
    newAsset.modelType=this.assetdetail.modelType;
    newAsset.serialNumber=this.assetdetail.serialNumber;
    newAsset.hostName=this.assetdetail.hostName;
    newAsset.osType=this.assetdetail.osType;
    newAsset.assetModelId=this.assetdetail.assetModelId;
    newAsset.dualBand=this.assetdetail.dualBand;
    newAsset.imeiNo=this.assetdetail.imeiNo;
    newAsset.ramUpgrade=this.assetdetail.ramUpgrade;
    newAsset.ramSerialNo=this.assetdetail.ramSerialNo;
    newAsset.ramPartNo=this.assetdetail.ramPartNo;
    newAsset.assetStatus=this.assetdetail.assetStatus;
    newAsset.vendor=this.assetdetail.vendor;
    newAsset.fiscalYear=this.assetdetail.fiscalYear;
    newAsset.complaint=this.assetdetail.complaint;
    newAsset.comments=this.assetdetail.comments;

    
  var response ={}
     
  this.assetDetailService.addAssetDetail(newAsset).subscribe(response=>{
console.log('The response after adding' + response);
 });
  
 this.assetList.push(newAsset);
  console.log(this.assetList.length);
  }

  editSelectedAssetDetail(template: TemplateRef<any>){

    this.modalRef.hide();
   }
   
   ngAfterViewInit(){
    let responseAssetList:assetdetail={};
   this.assetDetailService.getAsset().subscribe(responseAssetList=>{
     console.log(responseAssetList);
     this.assetDetailList = responseAssetList;
 });      
   }
   
   public openModal(template: TemplateRef<any>,i) {
    console.log(this.assetList[i].assetdetail);
    this.assetModal = this.assetList[i];
      this.modalRef = this.modelService.show(template);
     /* this.assetDetailService.addAssetDetail(this.assetModal).subscribe(response=>{
        console.log('The response after adding' + response);
         });*/
   }

}


export interface assetdetail{

	assetId?:number;
	modelType?:number;
  serialNumber?:string;
  hostName?:string;
  osType?:string;
  assetModelId?:String;
  dualBand?:string;
  imeiNo?:string;
  ramUpgrade?:string;
  ramSerialNo?:string;
  ramPartNo?:String;
	assetStatus?:string;
	vendor?:string;
  fiscalYear?:string;
  complaint?:String;
	comments?:String;
  
}

  


