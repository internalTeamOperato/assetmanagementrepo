import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  name:string;
  age:number;
  email:string;
  address:Address;
  hobbies:string[];
  hello:any;

  constructor() { 

  }

  ngOnInit() {
    this.name='pankaj';
    this.age=23;
    this.email='pankaj@gmail.com';
    this.address= {
      street:'taraiya',
      city:'madhubani',
      state:'bihar'
    }
    this.hobbies = ['sleeping','dancing','singing'];
    this.hello ='hello';

  }

}


interface Address{
  street:string;
  city:string;
  state:string;
}