import { Component, OnInit, AfterViewInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { AddAssetTypeService } from '../services/addassettype.service';
import { AddAssetModelService } from "../services/addassetmodel.service";
import { assetMasterData } from "../addassettype/addassettype.component";
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-addassetmodel',
  templateUrl: './addassetmodel.component.html',
  styleUrls: ['./addassetmodel.component.css'],
  providers: [AddAssetTypeService,AddAssetModelService]
})
export class AddassetmodelComponent implements OnInit {

  assetModel:AssetModel={};
  assetTypeList:assetMasterData[]=[];
  assetModelList:AssetModel[]=[];
  public modalRef: BsModalRef;

  constructor(private addassettypeservice:AddAssetTypeService,private addassetModelservice:AddAssetModelService, private modalService: BsModalService) { }

  ngOnInit() {
    this.assetModel.assetType = "Choose Asset Type"
    let responseAssetList:assetMasterData[]=[];
   this.addassettypeservice.getAssets().subscribe(responseAssetList=>{
     console.log(responseAssetList);
     this.assetTypeList = responseAssetList;
 }); 
  }


  ngAfterViewInit(){
    let responseAssetModelList:AssetModel[]=[];
   this.addassetModelservice.getAssetModels().subscribe(responseAssetList=>{
     console.log(responseAssetList);
     this.assetModelList = responseAssetList;
 });      
   }


   addAssetModel(){
     //set asset type id
   for (let index = 0; index < this.assetTypeList.length; index++) {
     const element = this.assetTypeList[index];
     if(element.assetType===this.assetModel.assetType){
      this.assetModel.assetTypeId = element.assetTypeId;
    }
   }
   let newAssetModel:AssetModel = {};
    newAssetModel = { ...this.assetModel };
var response = {};
  // Service call for adding the new asset to the DB
 this.addassetModelservice.addAssetModel(newAssetModel).subscribe(response=>{
console.log('The response after adding' + response);
 });

  this.assetModelList.push(newAssetModel);
  console.log(this.assetModelList.length);
  this.assetModel = {};
  this.assetModel.assetType = 'Choose Asset Type';
  }
  editSelectedAssetModel(template: TemplateRef<any>){

    this.modalRef.hide();
   }
   public openModal(template: TemplateRef<any>,i) {
    console.log(this.assetModel[i].assetType);
    this.assetTypeList = this.assetModel[i];
      this.modalRef = this.modalService.show(template);
    }

}

export interface AssetModel{
  modelName?:string;
  assetMake?:string;
  modelImportDate?:Date;
  assetTypeId?:number;
  assetType?:string;
  partNumber?:string;
  modelType?:string;
  vendor?:string;
  activeStatus?:string;
  additionalAttributes?:string;
}