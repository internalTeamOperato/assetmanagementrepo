import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddassetmodelComponent } from './addassetmodel.component';

describe('AddassetmodelComponent', () => {
  let component: AddassetmodelComponent;
  let fixture: ComponentFixture<AddassetmodelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddassetmodelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddassetmodelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
