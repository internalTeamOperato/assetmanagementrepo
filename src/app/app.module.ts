import { LoginService } from './services/login.service';
//import { AuthGuard } from './uard.guard';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http'; 
import { RouterModule, Routes } from '@angular/router';
import { AppRoutes} from './app.routing';
import {DatepickerModule} from 'ngx-bootstrap/datepicker';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AlertModule } from 'ngx-bootstrap/alert';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AddassettypeComponent } from './addassettype/addassettype.component';
import { AddemployeeComponent } from './addemployee/addemployee.component';
import { EditemployeeComponent } from './editemployee/editemployee.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AllocdeallocComponent } from './allocdealloc/allocdealloc.component';
import { MATERIAL_SANITY_CHECKS } from '@angular/material';
import { RolemanagementComponent } from './rolemanagement/rolemanagement.component';
import { AddassetmodelComponent } from './addassetmodel/addassetmodel.component';
import { AddassetdetailComponent } from './addassetdetail/addassetdetail.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { LoginComponent } from './login/login.component';
import { EmployeeComponent } from './employee/employee.component';
import { DisplayComponent } from './display/display.component';
import { AuthguardGuard } from './authguard.guard';
import { UserService } from './user.service';



/* const appRoutes:Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'display',
    canActivate: [AuthguardGuard],
    component: DisplayComponent
  }
] */


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AddassettypeComponent,
    AddemployeeComponent,
    EditemployeeComponent,
    AllocdeallocComponent,
    RolemanagementComponent,
    AddassetmodelComponent,
    AddassetdetailComponent,
    SidebarComponent,
    LoginComponent,
    EmployeeComponent,
    DisplayComponent,
   
  ],
  imports: [
    BrowserModule,
    FormsModule,
   HttpModule,
   BrowserAnimationsModule,
   DatepickerModule.forRoot(),
    BsDropdownModule.forRoot(),
    AlertModule.forRoot(),
    ModalModule.forRoot(),
   RouterModule,
   RouterModule.forRoot(AppRoutes)
  ],
 providers:[ UserService,AuthguardGuard, LoginService],

            /*{
            provide: MATERIAL_SANITY_CHECKS,
            useValue: false
            }
       ],*/
  bootstrap: [AppComponent]
})
export class AppModule {

 }
