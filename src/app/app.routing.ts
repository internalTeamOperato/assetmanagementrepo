import { UserService } from './user.service';
import { AuthguardGuard } from './authguard.guard';
import { Routes } from '@angular/router';
import { HomeComponent } from "./home/home.component";
import { AddassettypeComponent } from "./addassettype/addassettype.component";
import { AddemployeeComponent } from "./addemployee/addemployee.component";
import {EditemployeeComponent } from "./editemployee/editemployee.component";
import {AllocdeallocComponent} from "./allocdealloc/allocdealloc.component";
import { RolemanagementComponent } from "./rolemanagement/rolemanagement.component";
import { AddassetmodelComponent } from "./addassetmodel/addassetmodel.component";
import { AddassetdetailComponent } from "./addassetdetail/addassetdetail.component";
import {LoginComponent } from "./login/login.component";
import {DisplayComponent } from "./display/display.component";

 
/*import {EmployeeComponent } from "./employee/employee.component";*/

export const AppRoutes:Routes = [


{path: '', component:LoginComponent },
{path: 'login', component:LoginComponent },
{path: 'display', component:DisplayComponent, canActivate:[AuthguardGuard]},
{path: 'home', component: HomeComponent },
{path: 'addassettype', component:AddassettypeComponent, canActivate:[AuthguardGuard]},
{path: 'addassetdetail', component:AddassetdetailComponent, canActivate:[AuthguardGuard]},
{path: 'addassetmodel', component:AddassetmodelComponent, canActivate:[AuthguardGuard] },
{path: 'addemployee', component:AddemployeeComponent , canActivate:[AuthguardGuard] },
{path: 'editemployee', component:EditemployeeComponent , canActivate:[AuthguardGuard] },
{path: 'allocdealloc', component:AllocdeallocComponent , canActivate:[AuthguardGuard] },
{path: 'rolemanagement', component:RolemanagementComponent , canActivate:[AuthguardGuard] }
];
