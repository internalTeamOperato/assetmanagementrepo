import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserService } from './user.service';
import { Router } from '@angular/router';

@Injectable()
export class AuthguardGuard implements CanActivate {


      constructor(private user: UserService, private router: Router)   {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    if(this.user.getUserLoggedIn()) {
      return true;
    } else {
      this.router.navigate(['login']);
      console.info(this.user.username);
      return true;
      
    }
   /*  
    console.log('you are not authenticated');
    return false; */
  }

  
    
  }
