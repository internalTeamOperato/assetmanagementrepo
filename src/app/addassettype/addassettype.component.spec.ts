import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddassettypeComponent } from './addassettype.component';

describe('AddassettypeComponent', () => {
  let component: AddassettypeComponent;
  let fixture: ComponentFixture<AddassettypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddassettypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddassettypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
