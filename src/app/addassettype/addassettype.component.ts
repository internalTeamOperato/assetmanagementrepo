import { Component, OnInit, AfterViewInit , TemplateRef} from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { Router } from '@angular/router';
import { AddAssetTypeService } from '../services/addassettype.service';


import * as $ from 'jquery';

@Component({
  selector: 'app-addassettype',
  templateUrl: './addassettype.component.html',
  styleUrls: ['./addassettype.component.css'],
  providers: [AddAssetTypeService]
})
export class AddassettypeComponent implements OnInit, AfterViewInit {
 
  assetType:string;
  assetIntroDate:Date;
  assetCategory:string;
  activeIndicator:string;
  comments:string;
  public alerts: any = [];

 
  assetList:assetMasterData[]=[];
  assetModal:assetMasterData = {};
  public modalRef: BsModalRef;
  

  ngOnInit() {
    this.assetCategory = "2";
    this.activeIndicator = "1";
    this.assetIntroDate = new Date();
   
    }
  

  backtohome(){

    this.router.navigate(['']);
  }

  onaddclick(){
 
    console.log(this.assetType);
    console.log(this.comments);
    console.log(this.assetIntroDate);
    console.log(this.assetCategory);
    console.log(this.activeIndicator);
    
    var newAsset:assetMasterData = {};

  newAsset.assetCategory = this.assetCategory;
 newAsset.activeIndicator = this.activeIndicator;
  newAsset.assetType = this.assetType;
  newAsset.assetIntroDate = this.assetIntroDate;
  newAsset.comments = this.comments;
  console.log(newAsset.assetCategory);
var response = {};
  // Service call for adding the new asset to the DB
 this.addassettypeservice.addAssetType(newAsset).subscribe(response=>{
console.log('The response after adding' + response);
 });

 this.alerts.push({
      type: 'info',
      msg: `Asset added successfully`,
      timeout: 5000
    });


  this.assetList.push(newAsset);
  console.log(this.assetList.length);
  }

  editSelectedAssetType(template: TemplateRef<any>){

 this.modalRef.hide();
}

  ngAfterViewInit(){
   let responseAssetList:assetMasterData[]=[];
  this.addassettypeservice.getAssets().subscribe(responseAssetList=>{
    console.log(responseAssetList);
    this.assetList = responseAssetList;
});      
  }

constructor(public router: Router, private modalService: BsModalService,
  private addassettypeservice:AddAssetTypeService) { 
}

public openModal(template: TemplateRef<any>,i) {
  console.log(this.assetList[i].assetType);
  this.assetModal = this.assetList[i];
    this.modalRef = this.modalService.show(template);
  }
  
}

export interface assetMasterData{
  assetTypeId?:number;
   assetType?:string;
  assetIntroDate?:Date;
  assetCategory?:string;
  activeIndicator?:string;
  comments?:string;
}
