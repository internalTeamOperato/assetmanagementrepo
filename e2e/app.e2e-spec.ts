import { AssetManagementUIPage } from './app.po';

describe('asset-management-ui App', () => {
  let page: AssetManagementUIPage;

  beforeEach(() => {
    page = new AssetManagementUIPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
